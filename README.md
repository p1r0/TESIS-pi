# TESIS-pi
SISTEMA DE OBTENCIÓN DE DATOS DEL COMPORTAMIENTO DE LA INTENSIDAD Y DINÁMICA DE ENERGÍA SOLAR

### Requerimientos
    - Raspberry pi 3.0
    - Raspbian (Sistema Operativo GNU/Linux)
    - Celda fotovoltaica

### Instalación

**Instalando python en sistema base**
```sh
$ sudo apt-get install build-essential python3 python3-dev python3-setuptools python3-pip
```
**Instalando bibliotecas necesarias**
```sh
$ sudo python3-spidev python3-rpi.gpio
$ sudo pip install pymongo==3.4.0
```
**habilitar modulo para comunicación SPI**
```sh
$ sudo raspi-config
```

### Diagramas

**Raspberry pi GPIO**
![Alt text](https://docs.microsoft.com/en-us/windows/iot-core/media/pinmappingsrpi/rp2_pinout.png "algo")

# Licencia

[**GPLV3**](./LICENSE).

# Enlaces a bibliotecas

**ADC:**
- [**MSP3208**](https://pypi.org/project/mcp3208/)

# Referencias

- [**Raspberry pi GPIO**](https://docs.microsoft.com/en-us/windows/iot-core/media/pinmappingsrpi/rp2_pinout.png)

- [**SPI**](http://hertaville.com/interfacing-an-spi-adc-mcp3008-chip-to-the-raspberry-pi-using-c.html)

# Información y contactos.

***Desarrolladores***
- David Eugenio Perez Negron Rocha [daroperez_negron@esimez.mx](mailto:daroperez_negron@esimez.mx)

### Por hacer

 - Primer aproximación.



  [//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

