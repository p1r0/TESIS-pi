#!/bin/bash
apt-get install python3
apt-get install python3-spidev
apt-get install python3-rpi.gpio
pip3 install pymongo==3.4.0
#activar el modulo para spi
raspi-config
#verificar que el modulo esta funcionado
lsmod | grep spidev
