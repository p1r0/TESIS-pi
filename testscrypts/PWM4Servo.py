import RPi.GPIO as GPIO
import time

#limpiamos al inicio
GPIO.cleanup()
#ajustes hechos al servo
#-90
limInferior=1.85
#0
limCero=5.75
#90
limSuperior=10.25
#pendiente de la recta
m = 0.046666 

#preparar puertos
#modo BOARD ??? 
GPIO.setmode(GPIO.BOARD)
#Ponemos el pin 7 como salida GPIO4
GPIO.setup(7,GPIO.OUT)   

#preparar servo
#creamos el objeto Servo a 50Hz
servo = GPIO.PWM(7, 50)
servo.start(limCero)

#conversor de grados a codigo de salida digital
grados = lambda x: (x - limInferior) / m - 90

imprime = lambda posicion: print('CSD:{:.2f} - grados:{:.2f}'.format(posicion, grados(posicion)))

try:
    posicion = limCero
    #lado izquierdo
    lado=False
    while True:
        #Probar limInferio, limCero y LimSuperior CALIBRACIÓN
        #NOTESE QUE EL CERO NO ES EXACTO, EL SERVOMOTOR NO ES LINEAL
        servo.ChangeDutyCycle(limInferior)
        imprime(limInferior)
        time.sleep(5)
        servo.ChangeDutyCycle(limCero)
        imprime(limCero)
        time.sleep(5)
        servo.ChangeDutyCycle(limSuperior)
        imprime(limSuperior)
        time.sleep(5)

        #Giro completo a baja velocidad
        #print('CSD:{:.2f} - grados:{:.2f}'.format(posicion, grados(posicion)))
        #if posicion > limInferior and lado == False:
        #    posicion-=.2
        #    servo.ChangeDutyCycle(posicion)
        #    time.sleep(1)
        #elif posicion < limSuperior and lado == True:
        #    posicion+=.2
        #    servo.ChangeDutyCycle(posicion)
        #    time.sleep(1)
        #else:
        #    lado = not lado

except KeyboardInterrupt:
    servo.stop()
    GPIO.cleanup()
