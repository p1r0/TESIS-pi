# sflaskAPI 

**simple REST flask API, integrates CORS ( dummy data as an example) with Example clients (Javascript, python3, others.)**

## Requirements
* python >= 3.5.3
* certifi==2018.11.29
* chardet==3.0.4
* Click==7.0
* Flask==1.0.2
* Flask-Cors==3.0.6
* idna==2.8
* itsdangerous==1.1.0
* Jinja2==2.10
* MarkupSafe==1.0
* pkg-resources==0.0.0
* requests==2.21.0
* six==1.11.0
* urllib3==1.24.1
* Werkzeug==0.14.1

## Instalation

**First you need to have the python base system requirements**
```sh
$ sudo apt-get install build-essential python3 python3-dev python3-setuptools python3-pip virtualenv
```
**clone the repo and switch to sflaskAPI directory:**
```sh
$ git clone https://gitlab.com/p1r0/sflaskapi.git
$ cd sflaskapi
```
**set the virtual enviroment**
```sh
$ virtualenv -p python3 myenv
$ source myenv/bin/activate
```

**install requirements.txt into the virtualenv:**
```sh
(myenv)$ pip3 install -r requirements.txt
```
**create a ssl certificate [run the createcert.sh script], NOTE: SKIP THIS IF YOU DON'T NEED SSL**
```sh 
(myenv)$ ./createcert.sh
Country Name (2 letter code) [AU]:MX
State or Province Name (full name) [Some-State]:MICH
Locality Name (eg, city) []:MORELIA
Organization Name (eg, company) [Internet Widgits Pty Ltd]:neetsec
Organizational Unit Name (eg, section) []:Security
Common Name (e.g. server FQDN or YOUR name) []:127.0.0.1
Email Address []:p1r0@127.0.0.1
```
> NOTE:
> for testing purposes we use the Common Name as 127.0.0.1
> so that we have no problems testing it locally.
> In most cases, this should be a domain name linked with the certificate

**Run the server:**
```sh
(myenv)$ python3 runServer.py
enable SSL?(YES/NO):YES
 * Debug mode: on
 * Running on https://0.0.0.0:8080/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
```

> NOTE: remember to set debug = False if you are going to deploy ;)
> FOR SECURITY REASONS


**In a second terminal run the client and get the data**
```sh
$ source myenv/bin/activate
(myenv)$ cd clients
(myenv)$ python3 clienteDatosSSL.py
```

*lets hope you get the output or something like this:*
```sh
1 for the post method
2 for the get method
anything else, close
input an option:1
{"Datos": [{"voltaje": [3.14159265359]}, {"angulo": [-3.14159265359]}]}
voltaje=3.14159265359
angulo=-3.14159265359
1 for the post method
2 for the get method
anything else, close
input an option:2
{"Datos": {"angulo": -22.03428919522186, "voltaje": 2.0879678631408343}}
voltaje=2.0879678631408343
angulo=-22.03428919522186
1 for the post method
2 for the get method
anything else, close
input an option:3
bye ;)
```

***
# License

[**GPLV3**](./LICENSE).

# Information and contacts.

***Developers***

- p1r0 [daroperez_negron@esimez.mx](mailto:daroperez_negron@esimez.mx)

### ToDo

 - Integrate more clients and support ssl as optional in clients.

## References:

[**Video RESFTUL API Example**](https://youtu.be/CjYKrbq8BCw)

[**SSL in the Server**](http://werkzeug.pocoo.org/docs/0.14/serving/)

[**SSL in Client**](http://steven.casagrande.io/articles/python-requests-and-ssl/)

