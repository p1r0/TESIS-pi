#!/bin/env python3
import RPi.GPIO as GPIO
from mcp3208 import MCP3208
from pymongo import MongoClient
from time import sleep
from datetime import datetime
import os

def formatoDatos(dataDic):
    plantilla = {"generado" : datetime.utcnow(), 
        "coordenadas" : { 
                "lat" : dataDic['lat'], 
                "lon" : dataDic['lon'] 
        },
        "ADC" : { 
                "sensorACSD" : dataDic['snA'], 
                "sensorBCSD" : dataDic['snB'], 
                "celdaCSD" : dataDic['cel'], 
                "VoltajeReferencia" : dataDic['vrf'] 
        }, 
        "servo" : { 
                "limInferior" : dataDic['lIn'], 
                "limCero" : dataDic['lC0'], 
                "limSuperior" : dataDic['lSu'], 
                "cicloDeTrabajo" : dataDic['pos'] 
        }, 
        "celda" : { 
                "voltajeMax" : dataDic['cvm'], 
                "multiplicadorCelda" : dataDic['mtp'] 
        }, 
        "calculados" : { 
                "voltajeCelda" : dataDic['Vcl'], 
                "voltajeSensorA" : dataDic['VsA'], 
                "voltajeSensorB" : dataDic['VsB'], 
                "gradosServo" : dataDic['Gpo']
        }
    }
    return plantilla

def main():
    #limpiar puertos
    GPIO.cleanup()

    '''Base de datos '''
    client = MongoClient()
    db = client.test

    '''CONSTANTES'''
    #diccionario contenedor de datos para mongoDB
    dataDic={}
    #ubicación gps
    lat = 19.432608
    lon = -99.133208 
    #ajustes hechos al servo
    #-90
    limInferior=1.85
    #0
    limCero=5.75
    #90
    limSuperior=10.25
    #paso del servo
    paso = .1
    #pendiente de la recta
    m = 0.046666 
    #sleep time 
    t = 0.05
    #count for save in mongo
    s = 20
    #voltaje de referencia para el ADC
    vADCref = 3.34
    #umbral para mantener el motor, entre valores de fotoresistencia
    #valor en codigo de salida digital
    umbral = 30 
    #multiplicador del divisor de voltaje en celda
    multip = 2
    #voltaje maximo que provee la celda. NOTA: considerar divisor y vADCref
    celVMax = 6.4

    '''CONFIGURACION DE PUERTOS!'''
    #preparar puerto de servomotor
    #modo BOARD 
    GPIO.setmode(GPIO.BOARD)
    #Ponemos el pin 7 como salida GPIO4
    GPIO.setup(7,GPIO.OUT)   

    #creamos el objeto Servo a 50Hz
    servo = GPIO.PWM(7, 50)
    #posicion inicial en 0
    posicion = limCero
    servo.start(posicion)

    #inicializamos objeto del ADC
    adc = MCP3208()
    #Foro Resistencia de 90
    sensorA = lambda: adc.read(0)
    #Fotor Resistencia lado -90
    sensorB = lambda: adc.read(1)
    #celda fotovoltaica
    celda = lambda: adc.read(2)


    #funcion lambda para valor en volts del ADC
    voltaje = lambda x: x * vADCref / 4096 
    #conversor de grados a codigo de salida digital
    grados = lambda x: (x - limInferior) / m - 90

    #inicializando el diccionario con los datos requeridos key 3 carac.
    dataDic['lat'] = lat
    dataDic['lon'] = lon
    dataDic['lIn'] = limInferior
    dataDic['lC0'] = limCero
    dataDic['lSu'] = limSuperior
    dataDic['vrf'] = vADCref
    dataDic['mtp'] = multip
    dataDic['cvm'] = celVMax

    try:
        while True: 
            if s == 0:
                #cada segundo actualiza datos para mongodb
                dataDic['cel'] = celda()
                dataDic['snA'] = sensorA()
                dataDic['snB'] = sensorB()
                dataDic['pos'] = posicion
                dataDic['Vcl'] = voltaje(celda()) * multip
                dataDic['VsA'] = voltaje(sensorA())
                dataDic['VsB'] = voltaje(sensorB())
                dataDic['Gpo'] = grados(posicion)
                #generamos plantilla de datos json
                jsn = formatoDatos(dataDic)
                #almacenamos mongo database
                dbLogs = db.datosSistema.insert(jsn)
                #limpia e imprime ;)
                os.system('clear')
                print('posicion:{:.2f},voltaje={:.2f}'.format(grados(posicion),
                    voltaje(celda()) * multip))
                print('sensorA():{:.2f},sensorB()={:.2f}'.format(sensorA(),
                    sensorB()))
                s = 20
            if sensorA() > sensorB() and (sensorA() - sensorB()) > umbral:
                if posicion < limInferior:
                    posicion = limInferior
                    servo.ChangeDutyCycle(posicion)
                    sleep(t)
                else:
                    posicion += paso
                    servo.ChangeDutyCycle(posicion)
                    sleep(t)
            elif sensorA() < sensorB() and (sensorB() - sensorA()) > umbral:
                if posicion > limSuperior:
                    posicion = limSuperior
                    servo.ChangeDutyCycle(posicion)
                    sleep(t)
                else:
                    posicion -= paso
                    servo.ChangeDutyCycle(posicion)
                    sleep(t)
            else:
                sleep(t) 
            s -= 1
    except KeyboardInterrupt:
        servo.stop()
        GPIO.cleanup()

if __name__ == '__main__':
    main()
